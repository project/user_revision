<?php

/**
 * @file
 * Install, update and uninstall functions for the user revision module.
 */
use Drupal\user\Entity\User;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_module_preinstall().
 */
function user_revision_module_preinstall($module) {
  if ($module == 'user_revision') {
    $all_users = &drupal_static('user_revision_all_users', array());
    foreach (User::loadMultiple() as $user) {
      $all_users[$user->id()] = $user->toArray();
    }

    /* @var $user_storage \Drupal\Core\Entity\Sql\SqlContentEntityStorage */
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $user_storage = $entity_type_manager->getStorage('user');
    $entity_type_manager->onEntityTypeDelete($user_storage->getEntityType());
  }
}

/**
 * Implements hook_install().
 */
function user_revision_install() {
  /* @var $user_storage \Drupal\Core\Entity\Sql\SqlContentEntityStorage */
  $entity_type_manager = \Drupal::service('entity_type.manager');
  $user_storage = $entity_type_manager->getStorage('user');
  $entity_type_manager->onEntityTypeCreate($user_storage->getEntityType());

  $all_users = &drupal_static('user_revision_all_users', array());
  foreach ($all_users as $id => $user) {
    $user_entity = User::create($user);
    $user_entity->set('revision_uid', $id);
    $user_entity->save();
  }
  drupal_static_reset('user_revision_all_users');
}

/**
 * Implements hook_entity_presave().
 */
function user_revision_entity_presave(EntityInterface $entity) {
  if ($entity->getEntityTypeId() == 'user') {
    $all_users = &drupal_static('user_revision_all_users', array());
    if (isset($all_users[$entity->id()]) && isset($all_users[$entity->id()]['pass'])) {
      $entity->set('pass', current($all_users[$entity->id()]['pass'])['value']);
    }
    if (isset($all_users[$entity->id()]) && isset($all_users[$entity->id()]['changed'])) {
      $entity->set('changed', current($all_users[$entity->id()]['changed'])['value']);
    }
  }
}

/**
 * Implements hook_uninstall().
 */
function user_revision_uninstall() {
  throw new \RuntimeException('This module cannot be uninstalled.');
}
